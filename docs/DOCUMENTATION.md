## Account

|API endpoint|Returning type|
|:-|:-|
|"/account"|Account|
|"/bank"|List\<AccountBank\>|
|"/achievements"|List\<AccountAchievements\>|
|"/dyes"|int[]|
|"/inventory"|AccountInventory|
|"/materials"|List\<AccountMaterials\>|
|"/mini"|?|
|"/skins"|int[]|
|"/wallet"|List\<AccountWallet\>|


## Characters

|API endpoint|Returning type|
|-|-|
|"/characters"|List\<string\>|
|"/id/equipment"|CharactersEquipment|
|"/id/inventory"|CharactersInventory|
|"/id/recipes"|CharactersRecipes|
|"/id/specializations"|CharactersSpecializations|

## Miscellaneous

|API endpoint|Returning type|
|-|-|
|"/tokeninfo"|TokenInfo|