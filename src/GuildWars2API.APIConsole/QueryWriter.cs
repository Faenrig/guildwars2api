﻿using GuildWars2API.Core;
using System.Threading;
using System.Threading.Tasks;

namespace GuildWars2API.APIConsole
{
    public class QueryWriter
    {
        private readonly GetAllGameDataQuery mGetAllGameDataQuery;

        public QueryWriter(GetAllGameDataQuery getAllGameDataQuery)
        {
            mGetAllGameDataQuery = getAllGameDataQuery;
        }

        public async Task<Account> GetAllData(CancellationToken cancellationToken)
        {
            var result = await mGetAllGameDataQuery.Execute(cancellationToken).ConfigureAwait(false);
            return result;
        }
    }
}
