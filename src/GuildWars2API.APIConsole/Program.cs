﻿using GuildWars2API.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace GuildWars2API.APIConsole
{
    class Program
    {
        public static IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">
        /// Arguments provided to the application call.
        /// </param>
        private static async Task Main(string[] args)
        {
            // Registers DI services
            ConfigureServices();

            var writer = new QueryWriter((GetAllGameDataQuery)ServiceProvider.GetService(typeof(GetAllGameDataQuery)));
            var data = await writer.GetAllData(new CancellationToken());

            foreach (var value in data.GetType().GetProperties())
                Console.WriteLine($"{value.Name, 12}: {value.GetValue(data)}");
        }

        /// <summary>
        /// Registers all services to the DI container.
        /// </summary>
        private static void ConfigureServices()
        {
            // Create a list of dependencies
            var services = new ServiceCollection();

            services.AddSingleton<GetAllGameDataQuery>();

            services.AddHttpClient<GuildWarsClient>("GuildWarsClient", c =>
            {
                c.BaseAddress = new Uri("https://api.guildwars2.com/v2/");
                c.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "");
            });

            services.AddSingleton<GuildWarsClientFactory>();

            // Build the DI container
            ServiceProvider = services.BuildServiceProvider();
        }

        /// <summary>
        /// Garbage collects any disposable services.
        /// </summary>
        private static void DisposeServices()
        {
            if (ServiceProvider == null)
                return;
            else if (ServiceProvider is IDisposable)
                ((IDisposable)ServiceProvider).Dispose();
        }
    }
}
