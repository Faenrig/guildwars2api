﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace GuildWars2API.Core
{
    public class GuildWarsClientFactory
    {
        #region Private Member

        /// <summary>
        /// The injected ServiceProvider.
        /// </summary>
        private readonly IServiceProvider mServiceProvider;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="serviceProvider"></param>
        public GuildWarsClientFactory(IServiceProvider serviceProvider)
        {
            mServiceProvider = serviceProvider;
        }

        #endregion

        /// <summary>
        /// Provides a <see cref="GuildWarsClient"/> from the DI container.
        /// </summary>
        public GuildWarsClient Create()
        {
            return mServiceProvider.GetRequiredService<GuildWarsClient>();
        }
    }
}
