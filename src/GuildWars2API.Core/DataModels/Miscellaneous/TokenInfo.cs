﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class TokenInfo
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("permissions")]
        public List<string> Permissions { get; set; }
    }
}
