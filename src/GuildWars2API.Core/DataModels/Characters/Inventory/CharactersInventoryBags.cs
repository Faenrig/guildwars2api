﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharactersInventoryBags
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("")]
        public int Size { get; set; }

        [JsonPropertyName("inventory")]
        public List<CharactersInventoryItem> Item { get; set; }
    }
}
