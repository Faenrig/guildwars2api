﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharacterInventoryItemStats
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("attributes")]
        public CharacterInventoryItemAttributes Attributes { get; set; }
    }
}
