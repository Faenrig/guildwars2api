﻿namespace GuildWars2API.Core
{
    public class CharacterInventoryItemAttributes
    {
        public int Power { get; set; }

        public int Precision { get; set; }

        public int Toughness { get; set; }

        public int Vitality { get; set; }

        public int CritDamage { get; set; }

        public int Healing { get; set; }

        public int ConditionDamage { get; set; }
    }
}
