﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharactersInventoryItem
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("count")]
        public int Count { get; set; }

        [JsonPropertyName("charges")]
        public int Charges { get; set; }

        [JsonPropertyName("binding")]
        public string Binding { get; set; }

        [JsonPropertyName("upgrades")]
        public List<int> Upgrades { get; set; }

        [JsonPropertyName("upgrade_slot_indices")]
        public List<int> UpgradeSlotIndices { get; set; }

        [JsonPropertyName("bound_to")]
        public string BoundTo { get; set; }

        [JsonPropertyName("dyes")]
        public List<int> Dyes { get; set; }
    }
}
