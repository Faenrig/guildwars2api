﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharactersEquipment
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("slot")]
        public string Slot { get; set; }

        [JsonPropertyName("binding")]
        public string Binding { get; set; }

        [JsonPropertyName("bound_to")]
        public string BoundTo { get; set; }
    }
}
