﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharactersRecipes
    {
        [JsonPropertyName("recipes")]
        public List<int> Recipes { get; set; }
    }
}
