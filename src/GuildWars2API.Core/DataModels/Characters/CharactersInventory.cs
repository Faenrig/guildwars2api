﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class CharactersInventory
    {
        [JsonPropertyName("bags")]
        public List<CharactersInventoryBags> Bags { get; set; }
    }
}
