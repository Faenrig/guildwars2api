﻿using System.Text;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class AccountBank
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("count")]
        public int Count { get; set; }

        [JsonPropertyName("binding")]
        public string Binding { get; set; }
    }
}
