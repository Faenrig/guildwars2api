﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class AccountAchievements
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("current")]
        public int Current { get; set; }

        [JsonPropertyName("max")]
        public int Max { get; set; }

        [JsonPropertyName("done")]
        public bool Done { get; set; }
    }
}
