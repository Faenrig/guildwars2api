﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class AccountInventory
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("count")]
        public int Count { get; set; }

        [JsonPropertyName("binding")]
        public string Binding { get; set; }
    }
}
