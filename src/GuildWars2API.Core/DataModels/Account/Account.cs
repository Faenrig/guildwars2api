﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class Account
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [JsonPropertyName("age")]
        public int Age { get; set; }
        
        [JsonPropertyName("world")]
        public int World { get; set; }
        
        [JsonPropertyName("guild")]
        public List<string> Guild { get; set; }
        
        [JsonPropertyName("guild_leader")]
        public List<string> GuildLeader { get; set; }
        
        [JsonPropertyName("created")]
        public DateTimeOffset Created { get; set; }
        
        [JsonPropertyName("access")]
        public List<string> Access { get; set; }
        
        [JsonPropertyName("commander")]
        public bool Commander { get; set; }
        
        [JsonPropertyName("fractal_level")]
        public int FractalLevel { get; set; }
        
        [JsonPropertyName("daily_ap")]
        public int DailyAp { get; set; }
        
        [JsonPropertyName("monthly_ap")]
        public int MonthlyAp { get; set; }
        
        [JsonPropertyName("wvw_rank")]
        public int WvwRank { get; set; }
    }
}
