﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class AccountWallet
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("value")]
        public int Value { get; set; }
    }
}
