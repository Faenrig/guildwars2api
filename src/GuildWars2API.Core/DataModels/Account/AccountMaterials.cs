﻿using System.Text.Json.Serialization;

namespace GuildWars2API.Core
{
    public class AccountMaterials
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("category")]
        public int Category { get; set; }

        [JsonPropertyName("count")]
        public int Count { get; set; }
    }
}
