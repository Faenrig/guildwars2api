﻿using System.Text.Json;

namespace GuildWars2API.Core
{
    /// <summary>
    /// Provides standard Json Serializer Options for common tasks.
    /// </summary>
    public static class DefaultJsonSerializerOptions
    {
        /// <summary>
        /// Sets the default Serializer Options so that the returned object's
        /// property names are case sensitive.
        /// </summary>
        public static JsonSerializerOptions Options => new JsonSerializerOptions { PropertyNameCaseInsensitive = false };
    }
}
