﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GuildWars2API.Core
{
    /// <summary>
    /// Provides the structure to execute API calls.
    /// </summary>
    public interface IApiQuery
    {
        /// <summary>
        /// Executes an API call.
        /// </summary>
        Task<Account> Execute(CancellationToken cancellationToken);
    }
}
