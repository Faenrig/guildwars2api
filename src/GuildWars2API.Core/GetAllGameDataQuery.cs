﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GuildWars2API.Core
{
    public class GetAllGameDataQuery : IApiQuery
    {
        #region Private Member

        /// <summary>
        /// The client factory for the Guild Wars 2 API.
        /// </summary>
        private readonly GuildWarsClientFactory mGuildWarsClientFactory;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetAllGameDataQuery(GuildWarsClientFactory guildWarsClientFactory)
        {
            mGuildWarsClientFactory = guildWarsClientFactory ?? throw new ArgumentNullException(nameof(guildWarsClientFactory));
        }

        #endregion
        
        /// <summary>
        /// Executes an API call.
        /// </summary>
        public async Task<Account> Execute(CancellationToken cancellationToken)
        {
            var guildWarsClient = mGuildWarsClientFactory.Create();
            var response = await guildWarsClient.GetAllData(cancellationToken).ConfigureAwait(false);
            return response;
        }
    }
}
