﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace GuildWars2API.Core
{
    public class GuildWarsClient
    {
        #region Private Member

        /// <summary>
        /// The HttpClient to call the Guild Wars 2 API.
        /// </summary>
        private readonly HttpClient mHttpClient;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GuildWarsClient(HttpClient httpClient)
        {
            mHttpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        #endregion

        /// <summary>
        /// Gets all data from the Guild Wars 2 API.
        /// </summary>
        public async Task<Account> GetAllData(CancellationToken cancellationToken)
        {
            // Prepare the request
            var request = CreateRequest();

            // Get a response from the API
            var result = await mHttpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);

            // Use the Streamresult coming from the API...
            using (var contentStream = await result.Content.ReadAsStreamAsync())
            {
                // Deserialize the Streamresult
                return await JsonSerializer.DeserializeAsync<Account>(contentStream, DefaultJsonSerializerOptions.Options, cancellationToken);
            }
        }

        /// <summary>
        /// Creates a HTTP GET request from an URL.
        /// </summary>
        private static HttpRequestMessage CreateRequest()
        {
            // Returns a HTTP GET request for the Guild Wars 2 API
            return new HttpRequestMessage(HttpMethod.Get, "account");
        }
    }
}
